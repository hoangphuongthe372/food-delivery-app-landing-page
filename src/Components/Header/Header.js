import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import {MenuItems} from "./MenuItems"

function Header() {

    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);

    return (
        <>
            <nav className="header__navbar">
                <div className="header__logo">
                    <Link to="/" className="header__logo-link">
                        <img src="./img/Bella Olonje logo 111 1.png" alt="" className="header__logo-img"/>
                    </Link>
                </div>

                <div className="header__menu-icon" onClick={handleClick}>
                    <i className={click ? 'fas fa-times' : 'fas fa-bars'}></i>
                </div>

                <ul className={click ? 'header__menu-list active' : 'header__menu-list'}>
                    {MenuItems.map((item, index) => {
                        return(
                            <li key={index} className={item.cNamesli}>
                                <Link to={item.url} className={item.cNames}>{item.title}</Link>
                            </li>
                        )
                    })}
                </ul>                        
            </nav>
        </>
    )
}

export default Header;

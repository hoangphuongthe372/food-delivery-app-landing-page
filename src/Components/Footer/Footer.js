import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            // <footer className="footer">
            //    <div className="grid wide footer__container"> 
            //         <div className="footer__container-content">
            //             <div className="footer__logo hide-on-mobile">
            //                 <a href="/#" className="footer__logo-link">
            //                     <img src="./img/Bella Olonje logo 111 1.png" alt="" className="footer__logo-img"/>
            //                 </a>
            //             </div>

            //             <ul className="footer__menu-list">
            //                 <li className="footer__menu-item">
            //                     <a href="/#" className="footer__menu-item--link">
            //                         <img src="./img/twitter.png" alt="" className="footer__menu-item--img"/>
            //                     </a>
            //                 </li>

            //                 <li className="footer__menu-item">
            //                     <a href="/#" className="footer__menu-item--link">
            //                         <img src="./img/fb.png" alt="" className="footer__menu-item--img"/>
            //                     </a>
            //                 </li>

            //                 <li className="footer__menu-item">
            //                     <a href="/#" className="footer__menu-item--link">
            //                         <img src="./img/ig.png" alt="" className="footer__menu-item--img"/>
            //                     </a>
            //                 </li>
            //             </ul>

            //             <div className="footer__content-copyright">
            //                 <p>Copywright 2020 Bella Onojie.com</p>
            //             </div>

            //         </div>
            //     </div>
            // </footer>
            <div className="footer">
                <div className="footer-icons">
                    <div className="row">
                        <div className="col-sm-4">
               <div className="image">
                <img src="./img/Bella Olonje logo 111 1.png"/>
                </div>
                </div>
                <div className="col-sm-4">
                   <div className="footer-dev">               
                    <i class="fa fa-twitter"></i>     
                    <i class="fa fa-facebook-official"></i> 
                    <i class="fa fa-instagram"></i>
               </div>
               </div>
                <div className="col-sm-4">
               <p className="devo">Just type what's on your mind and we'll</p>
               <p className="copy">
                   <i class=" fa fa-copyright"></i>
                   Copyright 2020 Bella Onojei.com
               </p>
               </div>
               </div>
               </div>
               </div> 
           
        );
    }
}

export default Footer;
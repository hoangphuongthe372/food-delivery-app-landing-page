
import React from 'react'

function Banner() {
    // const [div, setDiv] = useState(true);

    // const showHr = () => {
    //     if(window.innerWidth <= 960){
    //         setDiv(false);
    //     }else{
    //         setDiv(true);
    //     }
    // };

    // useEffect(() => {
    //     showHr();
    // }, []);

    // window.addEventListener('resize', showHr);
    return (
        <div className="banner">

            <div className="banner__content">
                <section className="banner__content-text">
                    <h3 className="banner__content-text--heading">Food app</h3>
                    <h1 className="banner__content-text--title">Why stay hungry when you can order form Bella Onojie</h1>
                    <h4 className="banner__content-text--des">Download the bella onoje’s food app now on</h4>
                </section>
                <div className="banner__content-btn">
                    <button className="btn btn-primary">Playstore</button>
                    <button className="btn btn-normal">App store</button>
                </div>

                
            </div>
            <div className="banner__img">
                <img src="./img/Rectangle.png" className="banner__img-1" alt="" />
                <img src="./img/Rectangle1.png" className="banner__img-2" alt="" />
            </div>

            {/* {div && <div className="banner__text">
                        <hr/>
                        <p>How the app works</p>
                    </div>
            } */}
            
            <div className="banner__text">
                <hr/>
                <p>How the app works</p>
            </div>

        </div>
    )
}

export default Banner

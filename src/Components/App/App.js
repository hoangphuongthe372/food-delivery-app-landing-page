import './App.css';
import './reponsive.css';
import React from 'react';
import Header from '../Header/Header';
import Banner from '../Banner/Banner';
import Contents from '../Contents/Contents'
import Footer from '../Footer/Footer';
import {BrowserRouter as Router} from 'react-router-dom'

function App() {
    return (
      <>
          <Router>
            <Header />
            <Banner />
            <Contents/>
            <Footer/>
          </Router>
      </>
    );
  }


export default App;

